﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidLessonBase
{
    public class ElectronicsInvoice
    {
        public Equipment Equipment { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }

        public ElectronicsInvoice(Equipment equipment, int quantity, double discount)
        {
            Equipment = equipment;
            Quantity = quantity;
            Discount = discount;
        }

        public double CalculateAmountDue()
        {
            return Equipment.Price * Quantity - (Equipment.Price * Discount);
        }

        public void DisplayInvoice()
        {
            Console.WriteLine($"{Equipment.Description, -10}{Equipment.Price, -5}{Quantity, -4}Total: {CalculateAmountDue()}" +
                $"\nDiscount Factor applied: {Discount}; Discount Type:{GetDiscountText()}");
        }

        public void SaveInvoiceToTextFile(string fileName)
        {
            using var fileWriter = new StreamWriter(fileName);

            fileWriter.WriteLine($"{Equipment.Description,-10}{Equipment.Price,-5}{Quantity,-4}Total: {CalculateAmountDue()}" +
                $"\nDiscount Factor applied: {Discount}; Discount Type:{GetDiscountText()}");
        }

        public string GetDiscountText()
        {
            switch (Discount)
            {
                case 0.05:
                    return "STUDENT DISCOUNT";
                case 0.1:
                    return "PENSIONER DISCOUNT";
                default:
                    return string.Empty;
            }
        }
    }
}
