﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidLessonBase
{
    public class Equipment
    {
        public double Price { get; set; }
        public string Description { get; set; }
    }
}
