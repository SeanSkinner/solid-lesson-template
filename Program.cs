﻿using System;

namespace SolidLessonBase
{
    class Program
    {
        static void Main(string[] args)
        {
            var printer = new Equipment { Description = "Canon MB404", Price = 500.0 };

            var invoice = new ElectronicsInvoice(printer, 5, 0.1);

            invoice.DisplayInvoice();
        }
    }
}
